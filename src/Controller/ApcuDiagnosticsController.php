<?php

namespace Drupal\apcu_diagnostics\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * A controller to load the APCu cache diangostics page.
 */
class ApcuDiagnosticsController extends ControllerBase {

  /**
   * Render the diagnostics page.
   */
  public function content(): array {
    $return = [];

    // apc.php uses $_SERVER['PHP_SELF'] to identify from where the code is
    // being executed. Override for the Drupal path.
    $php_self = $_SERVER['PHP_SELF'];
    $_SERVER['PHP_SELF'] = '/admin/reports/apcu';

    // Disable authentication as Drupal handles access via routing.
    define("USE_AUTHENTICATION", 0);
    // Define globals for apc.php script. For some reason, this is necessary
    // when including inside the controller.
    // phpcs:ignore
    global $MYREQUEST, $MY_SELF_WO_SORT, $MY_SELF, $AUTHENTICATED;

    // Find apc.php and load it.
    if (is_file(DRUPAL_ROOT . '/vendor/krakjoe/apcu/apc.php')) {
      require DRUPAL_ROOT . '/vendor/krakjoe/apcu/apc.php';
    }
    elseif (is_file(dirname(DRUPAL_ROOT, 1) . '/vendor/krakjoe/apcu/apc.php')) {
      require dirname(DRUPAL_ROOT, 1) . '/vendor/krakjoe/apcu/apc.php';
    }
    else {
      $return = [
        '#markup' => $this->t("The /vendor/krakjoe/apcu/apc.php file is missing. Please refer to this module's README.md file."),
      ];
    }

    // Reset $_SERVER['PHP_SELF'] to its original value.
    $_SERVER['PHP_SELF'] = $php_self;

    return $return;
  }

}
