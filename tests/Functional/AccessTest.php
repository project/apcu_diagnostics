<?php

namespace Drupal\Tests\apcu_diagnostics\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the global settings form.
 *
 * @group apcu_diagnostics
 */
class AccessTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The test administrative user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * The test non-administrative user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $nonAdminUser;

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'apcu_diagnostics',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create an admin user.
    $this->adminUser = $this
      ->drupalCreateUser([
        'access administration pages',
        'access apcu diagnostics',
      ]);
    // Create a non-admin user.
    $this->nonAdminUser = $this
      ->drupalCreateUser([
        'access administration pages',
      ]);
  }

  /**
   * Tests route permissions.
   */
  public function testRoutePermissions() {
    $assert_session = $this->assertSession();

    $this->drupalLogin($this->nonAdminUser);
    // Non-admin user is unable to access report page.
    $this->drupalGet('/admin/reports/apcu');
    $assert_session->statusCodeEquals(403);

    $this->drupalLogin($this->adminUser);
    // Admin user is unable to access report page.
    $this->drupalGet('/admin/reports/apcu');
    $assert_session->statusCodeEquals(200);
  }

}
