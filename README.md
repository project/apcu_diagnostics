The APCu Diagnostics module provides Drupal integration for the debug tools
provided by [krakjoe/apcu](https://github.com/krakjoe/apcu).

For a full description of the module, visit the
[project page](https://www.drupal.org/project/apcu_diagnostics).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/apcu_diagnostics).

## Table of contents

- Requirements
- Installation
- Usage
- Maintainers

## Requirements

This module requires the following Composer repository be added to the
project's composer.json file:

```
{
    "type": "package",
    "package": {
        "name": "krakjoe/apcu",
        "version": "5.1.23",
        "source": {
            "url": "https://github.com/krakjoe/apcu.git",
            "type": "git",
            "reference" : "origin/master"
        }
    }
}
```

For example:

```
"repositories": [
    {
        "type": "composer",
        "url": "https://packages.drupal.org/8"
    },
    {
        "type": "package",
        "package": {
            "name": "krakjoe/apcu",
            "version": "5.1.23",
            "source": {
                "url": "https://github.com/krakjoe/apcu.git",
                "type": "git",
                "reference" : "origin/master"
            }
        }
    }
],
```

## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

For the project, run `composer require krakjoe/apcu`.

## Configuration

1. Enable the module at Administration > Extend.
1. Grant the 'Access APCu diagnostics' permission to roles, as necessary.
   (Be mindful this is an administrator-level permission.)

## Usage

In the Admin menu, navigate to 'Reports' -> 'APCu diagnostics report'.

## Maintainer

- Chris Burge - [chrisburge](https://www.drupal.org/u/chris-burge)
